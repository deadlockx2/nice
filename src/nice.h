/**
 * NICE : Nearly Intelligible Communications Entity
 */
#ifndef _NICE_
#define _NICE_

#include "eval.h"
#include "socket.h"

namespace nicebot {
    class NICE {
        public:
            NICE() {
                channel = "#projectopencannibal";
                portnum = "6667";
                botnick = "NICE";
                realname = "NICE, a simple FOSS bot";
                irc_server = "irc.freenode.org";
            }

            string channel;
            string portnum;
            string botnick;
            string realname;
            string irc_server;

            void start() {
                bot_connect(
                    channel,
                    portnum,
                    botnick,
                    realname,
                    irc_server
                );
            }
    }; // class NICE
} // namespace nicebot

#endif // _NICE_
