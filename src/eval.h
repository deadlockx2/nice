/**
 * NICE : Nearly Intelligible Communications Entity
 *
 * Eval: Stream and command handling.
 *
 * TODO
 *  Import all basic commands from Hab.
 */
#ifndef _NICE_EVAL_
#define _NICE_EVAL_

#include "socket.h"

using namespace std;

#define MAXDATASIZE 100

namespace nicebot {
    bool check_string(string search, string search_string) {
        int len = search.size();
        int slen = search_string.size();

        // Iterate through search
        for(int i = 0; i < len; i++) {
            if(search_string[0] == search[i]) {
                bool found = true;
                for(int n = 1; n < slen; n++) {
                    if(search[i+n] != search_string[n]) {
                        found = false;
                    }
                }
                return found;
            }
        }
    }

    // Run our evaluation loop; all commands should/will be evaluated here.
    bool run_loop(int sock) {
        string buffer;
        while(1) {
            int bytes = recv(sock, *buffer, MAXDATASIZE-1, 0);
            cout << buffer;

            // If ping, pong.
            if(buffer.npos != buffer.find("PING")){
                pong(buffer);
            }

            /* Return false to back out of our loop if the connection is closed
             * or broken. */
            if(bytes == 0) {
                cout << "-----Connection Closed-----" << endl;
                cout << current() << endl;

                return false;
            }
        }
    }
} // namespace nicebot

#endif // _NICE_EVAL_
