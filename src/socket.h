/**
 * NICE : Nearly Intelligible Communications Entity
 *
 * Socket: connection, response and message handling.
 *
 * TODO
 *  Allow for connecting to multiple servers and multiple channels
 *      (should load from a separate header).
 */
#ifndef _NICE_SOCKET_
#define _NICE_SOCKET_

#include <cstdio>
#include <cstdlib>
#include <string>
#include <iostream>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <unistd.h>

//#include <arpa/inet.h>

#include "eval.h"

using namespace std;

namespace nicebot {
    int sock;
    bool run_loop(int sock);

    // Write a message to the socket.
    bool bot_write(string s) {
        cout<< "--> " + s << endl;
        int len = s.size();
        // Will a bot really send more then 512 characters, ever?
        char tmp[512];
        sprintf(tmp, "%s\r\n", s);
        int bytes = send(sock, s, len, 0);
        if(bytes == 0) {
            return false;
        } else {
            return true;
        }
    }

    // Get the current time.
    char *current() {
        time_t rawtime;
        struct tm *timeinfo;

        time(&rawtime);
        timeinfo = localtime(&rawtime);

        return asctime(timeinfo);
    }

    // Do I even need to explain this one? :P
    void pong(string buffer) {
        string search_string = "PING ";
        string ret = "PONG ";

        if(buffer.npos != buffer.find(search_string)) {
            found = true;
                    }

                    if(bot_write(ret) == false) {
                        cout << "timeout : " << current << endl;
                    }

                    return;
    }

    // Run our connection and start the evaluation loop.
    bool bot_connect(
        string chan,
        string port,
        string nick,
        string real,
        string server
    ) {
        struct addrinfo hints, *servinfo;

        // Ensure an empty struct.
        memset(&hints, 0, sizeof hints);

        // Setup socket variables
        hints.ai_family = AF_UNSPEC; // IPv4 or IPv6
        hints.ai_socktype = SOCK_STREAM;

        int res;
        if(res = getaddrinfo(server, port, &hints, &servinfo) != 0) {
            fprintf(stderr,"getaddrinfo: %s\n", gai_strerror(res));
            // If we can't setup our structs, return false and cancel connect.
            return false;
        }

        // Configure our socket and connect.
        if((sock = socket(servinfo->ai_family,
            servinfo->ai_socktype,
            servinfo->ai_protocol)) == -1) {
                perror("Socket error.");
                return false;
        }
        if(connect(sock, servinfo->ai_addr, servinfo->ai_addrlen) == -1) {
            close(sock);
            perror("Connection error.");
            return false;
        }
        freeaddrinfo(servinfo);

        /* This is waay larger of an array than we need but at 1 byte per
         * character I think we can spare a few ;) */
        char tmp[128];
        sprintf(tmp, "NICK %s", nick);
        bot_write(tmp);
        sprintf(tmp, "USER %s 0 * :%s", nick, real);
        bot_write(real);
        sprintf(tmp, "JOIN %s", chan);
        bot_write(tmp);

        // Finally run ourselves an evaluation loop.
        if(run_loop(sock) == false) {
            /* This will continue to run until the connection is closed
             * at which point it will close our socket and return false,
             * autmatically stopping our bot. */
            close(sock);
            return false;
        }
    }
} // namespace nicebot

#endif // _NICE_SOCKET_
