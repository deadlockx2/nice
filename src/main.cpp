/**
 * NICE : Nearly Intelligible Communications Entity
 */
#include "nice.h"

using namespace nicebot;

int main() {
	NICE bot = NICE();
	bot.start();

	return 0;
}
